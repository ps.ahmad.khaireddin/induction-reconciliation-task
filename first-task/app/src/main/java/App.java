import engine.ProvidersFactory;
import engine.SampleCompare;
import utils.Compare;
import utils.Provider;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(">> Enter source file location:");
        String sourcePath = scanner.nextLine();
        System.out.println(">> Enter source file format:");
        String sourceType = scanner.nextLine();
        System.out.println(">> Enter target file location:");
        String targetPath = scanner.nextLine();
        System.out.println(">> Enter target file format:");
        String targetType = scanner.nextLine();
        ProvidersFactory providersFactory = new ProvidersFactory();
        Provider sourceProvider = providersFactory.getProvider(sourcePath, sourceType);
        Provider targetProvider = providersFactory.getProvider(targetPath, targetType);
        Compare sampleCompare = new SampleCompare();
        // TODO send those parameters through doCompare and return the result path
        sampleCompare.setProviders(sourceProvider, targetProvider);
        sampleCompare.doCompare();
        System.out.println(sampleCompare.getAbsolutePath());
    }
}
