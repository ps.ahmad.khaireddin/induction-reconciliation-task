package utils;

import engine.Record;
import exception.DateFormatsException;
import exception.FileException;
import exception.NumberFormatsException;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;

public class JSONProvider extends Provider<JsonObject> {
    private File file;
    private JsonArray jsonArray;
    private JSONValidator jsonValidator = new JSONValidator();

    public JSONProvider(String path) {
        PathValidator.isValidPath(path);
        file = new File(path);
        try (InputStream inputStream = new FileInputStream(file);
             JsonReader jsonReader = Json.createReader(inputStream)) {
            jsonArray = jsonReader.readArray();
        } catch (IOException e) {
            throw new FileException("Throw when read JSON file", e);
        }
    }

    @Override
    public List<Record> getDataFile() {
        // TODO accept in this method the path then do the whole processing of parsing and readin here
        // TODO apply the same for CSV
        ArrayList<Record> listOfRecord = new ArrayList<>();
        for (int counter = 0; counter < jsonArray.size(); counter++) {
            JsonObject jsonObject = jsonArray.getJsonObject(counter);
            listOfRecord.add(createRecord(jsonObject));
        }
        return listOfRecord;
    }

    @Override
    protected Record createRecord(JsonObject jsonObject) {
        jsonValidator.isValid(jsonObject);
        CurrencyValidator.isValidCurrencyCode(getCurrencyCode(jsonObject));
        return new Record(getTransactionID(jsonObject).trim(), getParseAmount(jsonObject),
                getCurrencyCode(jsonObject).trim(), getParseDate(jsonObject));
    }

    @Override
    protected String getTransactionID(JsonObject record) {
        return record.getString("reference");
    }

    @Override
    protected String getAmount(JsonObject record) {
        return record.getString("amount");
    }

    @Override
    protected String getCurrencyCode(JsonObject record) {
        return record.getString("currencyCode");
    }

    @Override
    protected String getDate(JsonObject record) {
        return record.getString("date");
    }

    @Override
    protected Date getParseDate(JsonObject jsonObject) {
        try {
            return new SimpleDateFormat("dd/MM/yy").parse(getDate(jsonObject).trim());
        } catch (ParseException e) {
            throw new DateFormatsException("Record " + jsonObject.getString("reference") + " In JSON File have invalid date format", e);
        }
    }

    @Override
    protected BigDecimal getParseAmount(JsonObject jsonObject) {
        try {
            Currency currency = Currency.getInstance(getCurrencyCode(jsonObject).trim().toUpperCase());
            return new BigDecimal(getAmount(jsonObject)).setScale(currency.getDefaultFractionDigits());
        } catch (NumberFormatException e) {
            throw new NumberFormatsException("Record " + jsonObject.getString("reference")
                                             + " In JSON File have invalid amount format", e);
        }
    }
}
