package utils;

import javax.json.JsonObject;

public class JSONValidator implements Validator<JsonObject> {

    @Override
    public void isValid(JsonObject record) {
        if (check(record, "date")){
            throw new NullPointerException("in record " + record.getString("reference") + " you miss date key or value");
        }
        if (check(record, "reference")){
            throw new NullPointerException("in record " + record.getString("reference") + " you miss reference key or value");
        }
        if (check(record, "amount")){
            throw new NullPointerException("in record " + record.getString("reference") + " you miss amount key or value");
        }
        if (check(record, "currencyCode")){
            throw new NullPointerException("in record " + record.getString("reference") + " you miss currencyCode key or value");
        }
        if (check(record, "purpose")){
            throw new NullPointerException("in record " + record.getString("reference") + " you miss purpose key or value");
        }
    }

    private boolean check(JsonObject jsonObject, String key) {
        boolean validFile = !jsonObject.containsKey(key) || jsonObject.isNull(key);
        if (validFile)
            return true;

        if (!key.equals("purpose"))
            return jsonObject.getString(key).equals("");

        return false;
    }
}
