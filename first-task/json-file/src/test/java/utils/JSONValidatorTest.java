package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class JSONValidatorTest {
    @Test
    public void givenNullKey_whenIsValidRecord_thenReturnNullPointerException() throws FileNotFoundException {
        File file = new File("null-key-online-banking-transactions.json");
        InputStream is = new FileInputStream(file);
        JsonReader jsonReader = Json.createReader(is);
        JsonArray jsonArray = jsonReader.readArray();
        JsonObject jsonObject = jsonArray.getJsonObject(0);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JSONValidator().isValid(jsonObject));
        Assertions.assertEquals("in record TR-47884222201 you miss currencyCode key or value", exception.getMessage());
    }

    @Test
    public void givenNullValue_whenIsValidRecord_thenReturnNullPointerException() throws FileNotFoundException {
        File file = new File("null-value-online-banking-transactions.json");
        InputStream is = new FileInputStream(file);
        JsonReader jsonReader = Json.createReader(is);
        JsonArray jsonArray = jsonReader.readArray();
        JsonObject jsonObject = jsonArray.getJsonObject(0);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JSONValidator().isValid(jsonObject));
        Assertions.assertEquals("in record TR-47884222201 you miss amount key or value", exception.getMessage());
    }

    @Test
    public void givenEmptyValue_whenIsValidRecord_thenReturnNullPointerException() throws FileNotFoundException {
        File file = new File("empty-value-online-banking-transactions.json");
        InputStream is = new FileInputStream(file);
        JsonReader jsonReader = Json.createReader(is);
        JsonArray jsonArray = jsonReader.readArray();
        JsonObject jsonObject = jsonArray.getJsonObject(0);
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> new JSONValidator().isValid(jsonObject));
        Assertions.assertEquals("in record TR-47884222201 you miss date key or value", exception.getMessage());
    }

    @Test
    public void givenValidFile_whenIsValid_thenReturn() throws FileNotFoundException {
        File file = new File("online-banking-transactions.json");
        InputStream is = new FileInputStream(file);
        JsonReader jsonReader = Json.createReader(is);
        JsonArray jsonArray = jsonReader.readArray();
        JsonObject jsonObject = jsonArray.getJsonObject(0);
        Assertions.assertDoesNotThrow(() -> new JSONValidator().isValid(jsonObject));
    }
}
