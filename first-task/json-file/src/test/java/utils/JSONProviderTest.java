package utils;

import engine.Record;
import exception.DateFormatsException;
import exception.NumberFormatsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class JSONProviderTest {

    @Test
    public void givenInvalidCSVSource_whenSet_thenFail() {
        String path = "invalid-amount-format.json";
        JSONProvider numberFormatProviderTest = new JSONProvider(path);
        NumberFormatsException numberFormatException = Assertions.assertThrows(NumberFormatsException.class,
                () -> numberFormatProviderTest.getDataFile());
        Assertions.assertEquals("Record TR-47884222205 In JSON File have invalid amount format", numberFormatException.getMessage());
        path = "invalid-date-format.json";
        JSONProvider dateFormatProviderTest = new JSONProvider(path);
        DateFormatsException dateFormatsException = Assertions.assertThrows(DateFormatsException.class,
                () -> dateFormatProviderTest.getDataFile());
        Assertions.assertEquals("Record TR-47884222201 In JSON File have invalid date format", dateFormatsException.getMessage());
    }

    @Test
    public void givenValidFile_whenRead_thenReturnResult() throws ParseException {
        String jsonPath = "./online-banking-transactions.json";
        JSONProvider jsonProvider = new JSONProvider(jsonPath);
        Record fileRecord = new Record("TR-47884222245", new BigDecimal(140).setScale(2), "USD",
                new SimpleDateFormat("dd/MM/yyyy").parse("20/01/2020"));
        Assertions.assertEquals(fileRecord.getAmount(), jsonProvider.getDataFile().get(0).getAmount());
        Assertions.assertEquals(fileRecord.getDate(), jsonProvider.getDataFile().get(0).getDate());
        Assertions.assertEquals(fileRecord.getTransUniqueID(), jsonProvider.getDataFile().get(6).getTransUniqueID());
    }
}
