package engine;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import utils.CSVProvider;
import utils.JSONProvider;
import utils.Provider;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class SampleCompareTest {

    @BeforeAll
    static void givenCompareFile_whenDoCompare_thenReturnResultInFile() {
        Provider csv = new CSVProvider("../sample-files/input-files/bank-transactions.csv");
        Provider json = new JSONProvider("../sample-files/input-files/online-banking-transactions.json");
        SampleCompare sampleCompare = new SampleCompare();
        sampleCompare.setProviders(csv, json);
        sampleCompare.doCompare();
        sampleCompare.getAbsolutePath();
        System.out.println(sampleCompare.getAbsolutePath());
    }

    @Test
    public void givenTwoFileMatched_whenCompare_thenCompareIfTwoFileIsMatch() throws IOException, ParseException {
        Reader myResult = Files.newBufferedReader(Paths.get("Matching Transactions file.csv"));
        CSVReader actualGenerated = new CSVReaderBuilder(myResult).withSkipLines(1).build();
        // TODO absolute paths
        Reader sampleResult = Files.newBufferedReader(Paths.get("/home/user/gitlab/first-task/sample-files/result-files/matching-transactions.csv"));
        CSVReader sampleCsvReader = new CSVReaderBuilder(sampleResult).withSkipLines(1).build();
        String[] csvRecord = actualGenerated.readNext();
        String[] sampleCsvRecord = sampleCsvReader.readNext();
        Record myRecord;
        Record sampleRecord;
        while (csvRecord != null && sampleCsvRecord != null) {
            myRecord = new Record(csvRecord[0], new BigDecimal(csvRecord[1]), csvRecord[2],
                    new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord[3]));

            sampleRecord = new Record(sampleCsvRecord[0], new BigDecimal(sampleCsvRecord[1]), sampleCsvRecord[2],
                    new SimpleDateFormat("yyyy-MM-dd").parse(sampleCsvRecord[3]));
            Assertions.assertTrue(myRecord.equals(sampleRecord));
            csvRecord = actualGenerated.readNext();
            sampleCsvRecord = sampleCsvReader.readNext();
        }
    }

    @Test
    public void givenTwoFileMismatching_whenCompare_thenCompareIfTwoFileIsMatch() throws IOException, ParseException {
        Reader myResult = Files.newBufferedReader(Paths.get("Mismatching Transactions file.csv"));
        CSVReader myCsvReader = new CSVReaderBuilder(myResult).withSkipLines(1).build();
        Reader sampleResult = Files.newBufferedReader(Paths.get("/home/user/gitlab/first-task/sample-files/result-files/mismatched-transactions.csv"));
        CSVReader sampleCsvReader = new CSVReaderBuilder(sampleResult).withSkipLines(1).build();
        String[] csvRecord = myCsvReader.readNext();
        String[] sampleCsvRecord = sampleCsvReader.readNext();
        Record myRecord;
        Record sampleRecord;
        while (csvRecord != null && sampleCsvRecord != null) {
            myRecord = new Record(csvRecord[1], new BigDecimal(csvRecord[2]), csvRecord[3],
                    new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord[4]));

            sampleRecord = new Record(sampleCsvRecord[1], new BigDecimal(sampleCsvRecord[2]), sampleCsvRecord[3],
                    new SimpleDateFormat("yyyy-MM-dd").parse(sampleCsvRecord[4]));
            Assertions.assertTrue(myRecord.equals(sampleRecord));
            csvRecord = myCsvReader.readNext();
            sampleCsvRecord = sampleCsvReader.readNext();
        }
    }

    @Test
    public void givenTwoFileMissing_whenCompare_thenCompareIfTwoFileIsMatch() throws IOException, ParseException {
        Reader myResult = Files.newBufferedReader(Paths.get("Missing Transactions file.csv"));
        CSVReader myCsvReader = new CSVReaderBuilder(myResult).withSkipLines(1).build();
        Reader sampleResult = Files.newBufferedReader(Paths.get("/home/user/gitlab/first-task/sample-files/result-files/missing-transactions.csv"));
        CSVReader sampleCsvReader = new CSVReaderBuilder(sampleResult).withSkipLines(1).build();
        String[] csvRecord = myCsvReader.readNext();
        String[] sampleCsvRecord = sampleCsvReader.readNext();
        Record myRecord;
        Record sampleRecord;
        while (csvRecord != null && sampleCsvRecord != null) {
            myRecord = new Record(csvRecord[1], new BigDecimal(csvRecord[2]), csvRecord[3],
                    new SimpleDateFormat("yyyy-MM-dd").parse(csvRecord[4]));

            sampleRecord = new Record(sampleCsvRecord[1], new BigDecimal(sampleCsvRecord[2]), sampleCsvRecord[3],
                    new SimpleDateFormat("yyyy-MM-dd").parse(sampleCsvRecord[4]));
            Assertions.assertTrue(myRecord.equals(sampleRecord));
            csvRecord = myCsvReader.readNext();
            sampleCsvRecord = sampleCsvReader.readNext();
        }
    }
}