package engine;

import exception.FileException;
import utils.Compare;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

public class SampleCompare extends Compare {

    private static String MATCHED_HEADER = "transaction id,amount,currency code,value date\n";
    private static String MISMATCHED_HEADER = "found in file,transaction id,amount,currency code,value date\n";

    @Override
    public void doCompare() {
        try (OutputStream osMatching = createFile("./Matching Transactions file.csv");
             OutputStream osMisMatching = createFile("./Mismatching Transactions file.csv");
             OutputStream osMissing = createFile("./Missing Transactions file.csv")) {

            writeToFile(osMatching, MATCHED_HEADER.getBytes());
            writeToFile(osMisMatching, MISMATCHED_HEADER.getBytes());
            writeToFile(osMissing, MISMATCHED_HEADER.getBytes());

            Insert insert = new Insert(osMatching, osMisMatching, osMissing);
            insert.insertInCorrectFile();
            getMissingTargetTransaction(osMissing);
        } catch (IOException e) {
            throw new FileException("Can not read file", e);
        }
    }


    private void getMissingTargetTransaction(OutputStream osMissing) {
        getMapTarget().forEach((k, v) -> {
            writeToFile(osMissing, "TARGET,".getBytes());
            writeToFile(osMissing, (getIDRecord(getMapTarget().get(k)) + "," + getAmount(getMapTarget().get(k)) + ","
                                    + getCurrencyCode(getMapTarget().get(k)) + "," + getValidDate(getMapTarget().get(k)) + "\n").getBytes());
        });
    }

    private class Insert {

        private OutputStream osMatching, osMisMatching, osMissing;

        public Insert(OutputStream osMatching, OutputStream osMisMatching, OutputStream osMissing) {
            this.osMatching = osMatching;
            this.osMisMatching = osMisMatching;
            this.osMissing = osMissing;
        }

        private void insertInCorrectFile() {
            List<Record> sourceDataFile = getSourceDataFile();
            for (int i = 0; i < sourceDataFile.size(); i++) {
                Map<String, Record> targetRecords = getMapTarget();
                Record sourceRecord = sourceDataFile.get(i);
                String sourceId = getIDRecord(sourceRecord);

                if (targetRecords.containsKey(sourceId)){
                    if (sourceRecord.equals(targetRecords.get(sourceId))){
                        writeToFile(osMatching, processingDataSource(i));
                    } else {
                        writeToFile(osMisMatching, "SOURCE,".getBytes());
                        writeToFile(osMisMatching, processingDataSource(i));
                        writeToFile(osMisMatching, "TARGET,".getBytes());
                        writeToFile(osMisMatching, processingDataTarget(i));
                    }
                    targetRecords.remove(sourceId);
                    continue;
                }
                writeToFile(osMissing, "SOURCE,".getBytes());
                writeToFile(osMissing, processingDataSource(i));
            }
        }
    }
}