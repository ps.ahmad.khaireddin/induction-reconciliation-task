package utils;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import engine.Record;
import exception.DateFormatsException;
import exception.FileException;
import exception.NumberFormatsException;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;

public class CSVProvider extends Provider<String[]> implements AutoCloseable {
    private CSVReader csvReader;
    private Reader reader;
    private CSVValidator csvValidator;

    public CSVProvider(String path) {
        try {
            PathValidator.isValidPath(path);
            reader = Files.newBufferedReader(Paths.get(path));
            csvReader = new CSVReaderBuilder(reader).build();
            int headerCount = csvReader.readNext().length;
            csvValidator = new CSVValidator(headerCount);
        } catch (IOException e) {
            throw new FileException("throw when read CSV file", e);
        }
    }

    @Override
    public List<Record> getDataFile() {
        ArrayList<Record> listOfRecord = new ArrayList<>();
        String[] line;
        while ((line = getLine(csvReader)) != null) {
            listOfRecord.add(createRecord(line));
        }
        return listOfRecord;
    }

    @Override
    protected Record createRecord(String[] line) {
        csvValidator.isValid(line);
        CurrencyValidator.isValidCurrencyCode(getCurrencyCode(line));
        return new Record(getTransactionID(line).trim(), getParseAmount(line),
                getCurrencyCode(line).trim(), getParseDate(line));
    }

    @Override
    protected Date getParseDate(String[] record) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(getDate(record).trim());
        } catch (ParseException e) {
            throw new DateFormatsException("Record " + getTransactionID(record) + " In CSV File have invalid date format", e);
        }
    }

    @Override
    protected BigDecimal getParseAmount(String[] record) {
        try {
            Currency currency = Currency.getInstance(getCurrencyCode(record).trim().toUpperCase());
            return new BigDecimal(getAmount(record)).setScale(currency.getDefaultFractionDigits());
        } catch (NumberFormatException e) {
            throw new NumberFormatsException("Record " + getTransactionID(record) + " In CSV File have invalid amount format", e);
        }
    }

    @Override
    protected String getTransactionID(String[] record) {
        return record[0];
    }

    @Override
    protected String getAmount(String[] record) {
        return record[2];
    }

    @Override
    protected String getCurrencyCode(String[] record) {
        return record[3];
    }

    @Override
    protected String getDate(String[] record) {
        return record[5];
    }

    private String[] getLine(CSVReader csvReader) {
        try {
            return csvReader.readNext();
        } catch (IOException e) {
            throw new FileException("Throw when read line in CSV file", e);
        }
    }

    @Override
    public void close() {
        try {
            csvReader.close();
        } catch (IOException e) {
            throw new FileException("throw when close CSV file", e);
        }
    }
}
