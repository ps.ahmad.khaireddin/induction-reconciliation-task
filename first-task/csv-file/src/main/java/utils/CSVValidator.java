package utils;

public class CSVValidator implements Validator<String[]> {
    private int headerCount;

    public CSVValidator(int headerCount) {
        this.headerCount = headerCount;
    }

    @Override
    public void isValid(String[] record) {
        if (record.length < headerCount){
            throw new IllegalStateException("Number of column in record " + record[0] + " is less than seven");
        }
        if (record.length > headerCount){
            throw new IllegalStateException("Number of column in record " + record[0] + " is greater than seven");
        }
    }
}