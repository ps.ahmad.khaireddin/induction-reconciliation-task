package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CSVValidatorTest {
    @Test
    public void givenInvalidFile_whenIsValid_thenThrowIllegalStateException() {
        String[] lessThanSeven = {"TR-47884222203", "2", "3", "4"};
        String[] greaterThanSeven = {"TR-47884222203", "2", "3", "4", "5", "6", "7", "8"};
        IllegalStateException invalidLength = Assertions.assertThrows(IllegalStateException.class,
                () -> new CSVValidator(7).isValid(lessThanSeven));
        Assertions.assertEquals("Number of column in record TR-47884222203 is less than seven", invalidLength.getMessage());
        invalidLength = Assertions.assertThrows(IllegalStateException.class,
                () -> new CSVValidator(7).isValid(greaterThanSeven));
        Assertions.assertEquals("Number of column in record TR-47884222203 is greater than seven", invalidLength.getMessage());

    }

    @Test
    public void givenValidFile_whenIsValid_thenReturn() {
        Assertions.assertDoesNotThrow(() -> new CSVProvider("bank-transactions.csv").getDataFile());
    }
}
