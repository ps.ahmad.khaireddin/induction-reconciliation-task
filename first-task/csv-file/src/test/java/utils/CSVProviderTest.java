package utils;

import engine.Record;
import exception.DateFormatsException;
import exception.NumberFormatsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class CSVProviderTest {

    @Test
    public void givenValidCSVSource_whenSet_thenReturnAllFileRecordAsArray() throws ParseException {
        String path = "bank-transactions.csv";
        CSVProvider provider = new CSVProvider(path);
        Record fileRecord = new Record("TR-47884222201", new BigDecimal("1200.000"),
                "JOD", new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-31"));
        Assertions.assertEquals(fileRecord.getTransUniqueID(), provider.getDataFile().get(0).getTransUniqueID());
    }

    @Test
    public void givenInvalidCSV_whenSet_thenFail() {
        String path = "number-exception.csv";
        CSVProvider numberFormatProviderTest = new CSVProvider(path);
        NumberFormatsException numberFormatException = Assertions.assertThrows(NumberFormatsException.class, () -> numberFormatProviderTest.getDataFile());
        Assertions.assertEquals("Record TR-47884222201 In CSV File have invalid amount format", numberFormatException.getMessage());
        path = "date-format-exception.csv";

        CSVProvider dateFormatProviderTest = new CSVProvider(path);
        DateFormatsException dateFormatsException = Assertions.assertThrows(DateFormatsException.class, () -> dateFormatProviderTest.getDataFile());
        Assertions.assertEquals("Record TR-47884222201 In CSV File have invalid date format", dateFormatsException.getMessage());
    }
}
