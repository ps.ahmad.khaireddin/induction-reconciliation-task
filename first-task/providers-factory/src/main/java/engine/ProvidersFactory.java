package engine;

import org.apache.commons.io.FilenameUtils;
import utils.CSVProvider;
import utils.JSONProvider;
import utils.PathValidator;
import utils.Provider;

public class ProvidersFactory {
    public Provider getProvider(String path, String type) {
        PathValidator.isValidPath(path);
        String extension = FilenameUtils.getExtension(path);
        if (type.equalsIgnoreCase(extension)){
            if (extension.equalsIgnoreCase("json")){
                return new JSONProvider(path);
            }
            if (extension.equalsIgnoreCase("csv")){
                return new CSVProvider(path);
            }
        }
        throw new IllegalStateException("please insert right type");
    }
}
