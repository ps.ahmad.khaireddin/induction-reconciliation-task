package engine;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.Provider;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ProvidersFactoryTest {
    @Test
    public void givenInValidType_whenGetSourceProvider_thenThrowIllegalStateException() throws FileNotFoundException {
        ProvidersFactory provider = new ProvidersFactory();
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class,
                () -> provider.getProvider("/home/user/gitlab/first-task/json-file/online-banking-transactions.json", "csv"));
        Assertions.assertEquals("please insert right type", exception.getMessage());
    }


    @Test
    public void givenValidType_whenGetSourceProvider_thenReturnProvider() throws IOException {
        ProvidersFactory provider = new ProvidersFactory();
        Provider json = provider.getProvider("/home/user/gitlab/first-task/json-file/online-banking-transactions.json", "json");
        Assertions.assertNotNull(json);
    }
}