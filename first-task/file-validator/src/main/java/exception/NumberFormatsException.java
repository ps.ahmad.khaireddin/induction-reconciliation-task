package exception;

public class NumberFormatsException extends RuntimeException {
    public NumberFormatsException(String massage, NumberFormatException exc) {
        super(massage, exc);
    }
}
