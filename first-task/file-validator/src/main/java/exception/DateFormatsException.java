package exception;

public class DateFormatsException extends RuntimeException {
    public DateFormatsException(String massage, Exception e) {
        super(massage, e);
    }
}
