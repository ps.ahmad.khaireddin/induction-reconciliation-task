package engine;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class Record {
    private String transUniqueID;
    private BigDecimal amount;
    private String currencyCode;
    private Date date;

    public Record(String transactionId, BigDecimal amount, String currencyCode, Date date) {
        this.transUniqueID = transactionId;
        this.amount = amount;
        this.currencyCode = currencyCode.toUpperCase();
        this.date = date;
    }

    public String getTransUniqueID() {
        return transUniqueID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record that = (Record) o;
        return Objects.equals(transUniqueID, that.transUniqueID) &&
               this.amount.compareTo(that.amount) == 0 &&
               Objects.equals(currencyCode, that.currencyCode) &&
               Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transUniqueID, amount, currencyCode, date);
    }
}
