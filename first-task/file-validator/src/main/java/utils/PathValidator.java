package utils;

import exception.FileException;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class PathValidator {
    public static void isValidPath(String path) {
        try {
            if (path == null)
                throw new NullPointerException("Null path");
            if (Files.notExists(Paths.get(path)))
                throw new FileNotFoundException("this path dose not exist");
            if (Files.isDirectory(Paths.get(path)))
                throw new IllegalArgumentException("Not file");
            if (!Files.isReadable(Paths.get(path)))
                throw new IllegalStateException("The file is not readable");
        } catch (FileNotFoundException e) {
                throw new FileException("File not found exception",e);
        }
    }
}
