package utils;

import engine.Record;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public abstract class Provider<PARAMETER_TYPE> {
    // TODO accept the path for the file
    public abstract List<Record> getDataFile();

    // TODO those are not part of the abstraction
    protected abstract Record createRecord(PARAMETER_TYPE record);

    protected abstract Date getParseDate(PARAMETER_TYPE date);

    protected abstract BigDecimal getParseAmount(PARAMETER_TYPE jsonObject);

    protected abstract String getTransactionID(PARAMETER_TYPE record);

    protected abstract String getAmount(PARAMETER_TYPE record);

    protected abstract String getCurrencyCode(PARAMETER_TYPE record);

    protected abstract String getDate(PARAMETER_TYPE record);
}
