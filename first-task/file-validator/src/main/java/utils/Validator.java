package utils;

@FunctionalInterface
public interface Validator<TYPE> {
    void isValid(TYPE record);
}
