package utils;

import engine.Record;
import exception.FileException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Compare {
    private String absolutePath;
    private Map<String, Record> targetMap = new HashMap<>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private List<Record> sourceDataFile;

    public abstract void doCompare();

    protected Map<String, Record> getMapTarget() {
        return targetMap;
    }

    public void setProviders(Provider source, Provider target) {
        sourceDataFile = source.getDataFile();
        setTargetMap(target.getDataFile());
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    protected OutputStream createFile(String path) {
        // TODO enhance, use Files.createFile
        File file = new File(path);
        try {
            file.createNewFile();
            absolutePath = file.getParentFile().getAbsolutePath();
            return new FileOutputStream(file);
        } catch (IOException e) {
            throw new FileException("Throw when create File in compare", e);
        }
    }

    protected void writeToFile(OutputStream outputStream, byte[] bytes) {
        try {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new FileException("can not write in the file",e);
        }
    }

    // TODO reimplement the record writing functionality
    protected byte[] processingDataSource(int i) {
        return (getIDRecord(getRecord(i)) + "," + getAmount(getRecord(i)) + ","
                + getCurrencyCode(getRecord(i)) + "," + getValidDate(getRecord(i)) + "\n").getBytes();
    }

    // TODO reimplement the record writing functionality

    protected byte[] processingDataTarget(int i) {
        return (targetMap.get(getIDRecord(getRecord(i))).getTransUniqueID() + "," + getAmountMap(i) + ","
                + targetMap.get(getIDRecord(getRecord(i))).getCurrencyCode() + "," + getDateFormat().format(targetMap.get(getIDRecord(getRecord(i))).getDate()) + "\n").getBytes();
    }

    protected List<Record> getSourceDataFile() {
        return sourceDataFile;
    }

    private BigDecimal getAmountMap(int i) {
        return getAmount(targetMap.get(getIDRecord(getRecord(i))));
    }

    private SimpleDateFormat getDateFormat() {
        return (SimpleDateFormat) dateFormat.clone();
    }

    protected Record getRecord(int i) {
        return (Record) sourceDataFile.get(i);
    }

    protected String getIDRecord(Record record) {
        return record.getTransUniqueID();
    }

    protected String getValidDate(Record fileRecord) {
        return dateFormat.format(fileRecord.getDate());
    }

    protected String getCurrencyCode(Record fileRecord) {
        return fileRecord.getCurrencyCode();
    }

    protected BigDecimal getAmount(Record fileRecord) {
        return fileRecord.getAmount();
    }

    private void setTargetMap(List<? extends Record> target) {
        for (Record fileRecord : target) {
            targetMap.put(getIDRecord(fileRecord), fileRecord);
        }
    }
}
