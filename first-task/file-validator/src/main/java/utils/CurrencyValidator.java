package utils;

public class CurrencyValidator {

    public static void isValidCurrencyCode(String currencyCode) {
        if (currencyCode.length() != 3) {
            throw new IllegalStateException("the currency code "+currencyCode+" dose not following ISO 4217 alphabetic code");
        }
    }
}
