package utils;

import exception.FileException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PathValidatorTest {
    @Test
    public void givenNullPath_whenConstructing_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> PathValidator.isValidPath(""));
        Assertions.assertEquals("Not file", exception.getMessage());
    }

    @Test
    public void givenNonExistSourcePath_whenConstructing_thenThrowFileNotFoundException() {
        FileException ex = Assertions.assertThrows(FileException.class,
                () -> PathValidator.isValidPath("alalksadklj"));
        Assertions.assertEquals("File not found exception", ex.getMessage());
    }
}
