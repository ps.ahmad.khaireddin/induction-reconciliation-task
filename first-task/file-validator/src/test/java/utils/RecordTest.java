package utils;

import engine.Record;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordTest {
    @Test
    public void givenValidAttributes_whenGetAttribute_thenReturnResult() throws ParseException {
        Record record = new Record("tr", new BigDecimal("140").setScale(2), "JOD",
                new SimpleDateFormat("yyyy-MM-dd").parse("1996-08-10"));
        Assertions.assertNotNull(record);
        String transUniqueID = record.getTransUniqueID();
        Date date = record.getDate();
        String currencyCode = record.getCurrencyCode();
        BigDecimal amount = record.getAmount();
        Assertions.assertEquals("tr", transUniqueID);
        Assertions.assertEquals(new BigDecimal("140").setScale(2), amount);
        Assertions.assertEquals(new SimpleDateFormat("yyyy-MM-dd").parse("1996-08-10"), date);
        Assertions.assertEquals("JOD", currencyCode);
    }
}
