package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CurrencyValidatorTest {
    @Test
    public void givenInvalidCurrency_whenIsValid_ThenThrowIllegalStateException(){
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> CurrencyValidator.isValidCurrencyCode("USDD"));
        Assertions.assertEquals("the currency code USDD dose not following ISO 4217 alphabetic code",exception.getMessage());
    }
    @Test
    public void givenValidCurrency_whenIsValid_thenDoNothing(){
        Assertions.assertDoesNotThrow(()->CurrencyValidator.isValidCurrencyCode("USD"));
    }
}
